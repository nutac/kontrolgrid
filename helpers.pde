/// COLORES
int ACTIVE=0;
int FOREGROUND=1;
int BACKGROUND=2;

color getColorFromIndex(int _index, int _uso) {
  int col=g.colores2[_index];
  color c=color(col);
  float r=red(c);
  float g=green(c);
  float b=blue(c);
  float multi=1;
  //float base=0;
  if (_uso==ACTIVE) {
    r+=20;
    g+=20;
    b+=20;
    //base=20;
    multi=1.6;
  } else if (_uso==FOREGROUND) {
    multi=1.3;
  } else if (_uso==BACKGROUND) {
    multi=0.5;
  }
  //c=color(base*multi+r*multi, base*multi+g*multi, base*multi+ b*multi);
  c=color(r*multi, g*multi, b*multi);
  return c;
}

boolean clickedOnMe(int x, int y, int ancho, int alto) {
  boolean respuesta = false;
  if ((mouseX >= x)  && (mouseX <= (x + ancho))) {
    if (mouseY >= y && mouseY <= (y + alto)) {
      respuesta = true;
    }
  }
  return respuesta;
}
/////////////////////////////

class ColorPicker {
  int x, y, width, height;
  int[] colors;
  boolean isShown = false;
  int selectedColor;

  ColorPicker(int _x, int _y, int[] _colors) {
    x = _x;
    y = _y;
    colors = _colors;
    width = colors.length * 20; // Asumiendo que cada color tiene un ancho de 20
    height = 20; // Asumiendo una altura fija
  }

  void show() {
    isShown = true;
  }

  void hide() {
    isShown = false;
  }

  boolean isShown() {
    return isShown;
  }

  void draw() {
    if (isShown) {
      for (int i = 0; i < colors.length; i++) {
        fill(colors[i]);
        rect(x + i * 20, y, 20, 20);
      }
    }
  }

  int getSelectedColor() {
    return selectedColor;
  }

  void mousePressed() {
    if (isShown) {
      int clickedIndex = (mouseX - x) / 20;
      println(selectedColor, clickedIndex);
      if (clickedIndex >= 0 && clickedIndex < colors.length) {
        selectedColor = clickedIndex;//colors[clickedIndex];
        
        hide();
      }
    }
  }
}
