void midiSendPause(int duration) {
  g.midiPaused = true;
  g.midiPauseStartTime = millis();
  g.midiPauseDuration = duration;
  //println(millis(), " paused");
}
void checkIfMidiIsPaused() {
  if (g.midiPaused) {
    if (millis() - g.midiPauseStartTime >= g.midiPauseDuration) {
      g.midiPaused = false;
      //println(millis(), " continue");
    }
  }
}
///////////////////////////////////////////////////////////////////////////
class Controller {
  int selector;
  int knobStart, knobEnd;
  int sliderStart, sliderEnd;
  int modeToggler;

  Controller(int selector, int knobStart, int knobEnd,
    int sliderStart, int sliderEnd, int modeToggler) {
    this.selector = selector;
    this.knobStart = knobStart;
    this.knobEnd = knobEnd;
    this.sliderStart = sliderStart;
    this.sliderEnd = sliderEnd;
    this.modeToggler = modeToggler;
  }
}

Controller[] controles = {
  new Controller(22, 14, 21, 2, 12, 47), // Valores para nanoKONTROL1
  new Controller(23, 16, 22, 0, 7, 43)  // Valores para nanoKONTROL2
};
//////////////////////////////////////////////////////////////////////

int nanoVersion = 0; // Default for nanoKONTROL1

import themidibus.*;
MidiBus midi;

void setupMidi() {
  //MidiBus.list();

  String[] devices = MidiBus.availableInputs();
  int nanoKontrolIndex = -1;
  for (int i = 0; i < devices.length; i++) {
    if (devices[i].contains("nanoKONTROL2")) {
      nanoKontrolIndex = i;
      nanoVersion = 1;
      break;
    } else if (devices[i].contains("nanoKONTROL")) {
      nanoKontrolIndex = i;
      nanoVersion = 0;
      break;
    }
  }

  if (nanoKontrolIndex != -1) {
    midi = new MidiBus(this, devices[nanoKontrolIndex], "LoopBe Internal MIDI");
  } else {
    println("nanoKONTROL no encontrado !!!");
  }
}

void controllerChange(int channel, int number, int value) {
  if (number == controles[nanoVersion].selector) {
    int index = int(map(value, 0, 127, 0, g.totalRows - 1));
    setRowSelected(index);
  } else if (number >= controles[nanoVersion].knobStart && number <= controles[nanoVersion].knobEnd) {
    int index = number - controles[nanoVersion].knobStart;
    setPotValue(selectedRow, index, value);
  } else if (number >= controles[nanoVersion].sliderStart && number <= controles[nanoVersion].sliderEnd) {
    int index=number - controles[nanoVersion].sliderStart;
    if (nanoVersion==0) {
      if (number>=8) index--;
      if (number>=12) index-=2;
    }
    setSliderValue(selectedRow, index, value);
  } else if (number == controles[nanoVersion].modeToggler) {
    if (value == 127) {
      toggleMode();
    }
  } else {
    println("Controller change: " + channel + ", " + number + ", " + value);
  }
  //println("Controller change: " + channel + ", " + number + ", " + value);
}


void toggleMode() {
  if (g.mode==KNOBS_GRID) {
    g.mode=SLIDERS_GRID;
  } else {
    g.mode=KNOBS_GRID;
  }
  for (int i=0; i<rows.size(); i++) {
    rows.get(i).setMode(g.mode);
  }
  setRowSelected(selectedRow);
}
int getControllerIndex(int _row, int _index) {
  return (int) _index+(_row*g.potsPerRow);
}

void sendMidi(int _id, int _valor) {
  if (!g.midiPaused) {
    int channel=g.channel-1;
    midi.sendControllerChange(channel, _id, _valor);
    println(channel, _id, _valor);
  }
}
