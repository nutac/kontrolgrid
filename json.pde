//int currentPresetIndex;
//void setPresetSelected(int index) {
//  // Guardar los valores actuales en el preset anterior
//  saveCurrentValuesToGlobalData(currentPresetIndex);

//  // Cargar los valores del nuevo preset
//  loadValuesFromGlobalData(index);

//  // Actualizar el preset actual
//  currentPresetIndex = index;
//}

JSONObject globalDataToJSON() {
  JSONObject json = new JSONObject();
  JSONArray presetsArray = new JSONArray();

  for (int i = 0; i < globalData.presets.size(); i++) {
    PresetData presetData = globalData.presets.get(i);
    JSONObject presetObj = new JSONObject();
    presetObj.setInt("channel", globalData.presets.get(i).channel);
    JSONArray rowsArray = new JSONArray();

    for (int j = 0; j < presetData.rows.size(); j++) {
      RowData rowData = presetData.rows.get(j);
      JSONObject rowObj = new JSONObject();
      rowObj.setString("caption", rowData.caption);
      rowObj.setInt("colour", rowData.colour);
      JSONArray potsArray = new JSONArray();

      for (int k = 0; k < rowData.pots.size(); k++) {
        PotData potData = rowData.pots.get(k);
        JSONObject potObj = new JSONObject();
        potObj.setString("potCaption", potData.potCaption);
        potObj.setString("sliderCaption", potData.sliderCaption);
        potObj.setInt("potValue", potData.potValue);
        potObj.setInt("sliderValue", potData.sliderValue);
        potObj.setInt("cc_knob", potData.cc_knob);
        potObj.setInt("cc_slider", potData.cc_slider);
        potObj.setInt("potColour", potData.potColour);
        potObj.setInt("sliderColour", potData.sliderColour);
        potsArray.append(potObj);
      }

      rowObj.setJSONArray("pots", potsArray);
      rowsArray.append(rowObj);
    }

    presetObj.setJSONArray("rows", rowsArray);
    presetsArray.append(presetObj);
  }



  json.setJSONArray("presets", presetsArray);

  return json;
}


void saveCurrentValuesToGlobalData(int presetIndex) {
  PresetData presetData = globalData.presets.get(presetIndex);
  //presetData.channel=globalData.presets.get(presetIndex).channel;
  presetData.channel = g.channel;
  println("channel saved: ", presetData.channel);
  for (int i = 0; i < rows.size(); i++) {
    Row row = rows.get(i);
    RowData rowData = presetData.rows.get(i);
    rowData.caption = row.caption;
    rowData.colour = row.colour;
    for (int j = 0; j < row.pots.size(); j++) {
      Pot pot = row.pots.get(j);
      PotData potData = rowData.pots.get(j);

      potData.potCaption = pot.potCaption;
      potData.sliderCaption = pot.sliderCaption;
      potData.potValue = pot.potValue;
      potData.sliderValue = pot.sliderValue;
      potData.cc_knob = pot.cc_knob;
      potData.cc_slider = pot.cc_slider;
      potData.potColour = pot.potColour;
      potData.sliderColour = pot.sliderColour;
    }
  }
}


void loadGlobalDataFromFile() {
  JSONObject json = loadJSONObject("data/new-data.json");
  JSONArray presetsArray = json.getJSONArray("presets");

  for (int i = 0; i < presetsArray.size(); i++) {
    JSONObject presetObj = presetsArray.getJSONObject(i);
    PresetData presetData = globalData.presets.get(i);
    //if (presetData.channel>-1) {
    presetData.channel = presetObj.getInt("channel");
    //}
    println(i, presetData.channel);
    JSONArray rowsArray = presetObj.getJSONArray("rows");

    for (int j = 0; j < rowsArray.size(); j++) {
      JSONObject rowObj = rowsArray.getJSONObject(j);
      RowData rowData = presetData.rows.get(j);
      rowData.caption = rowObj.getString("caption");
      rowData.colour = rowObj.getInt("colour");
      JSONArray potsArray = rowObj.getJSONArray("pots");

      for (int k = 0; k < potsArray.size(); k++) {
        JSONObject potObj = potsArray.getJSONObject(k);
        PotData potData = rowData.pots.get(k);
        potData.potCaption = potObj.getString("potCaption");
        potData.sliderCaption = potObj.getString("sliderCaption");
        potData.potValue = potObj.getInt("potValue");
        potData.sliderValue = potObj.getInt("sliderValue");
        potData.cc_knob = potObj.getInt("cc_knob");
        potData.cc_slider = potObj.getInt("cc_slider");
        potData.potColour = potObj.getInt("potColour");
        potData.sliderColour = potObj.getInt("sliderColour");
      }
    }
  }
}

void loadValuesFromGlobalData(int presetIndex) {
  println("--------LOADING");
  PresetData presetData = globalData.presets.get(presetIndex);
  //if(presetData.channel>-1){
  setChannel(presetData.channel);
  println("channel loaded: ", presetData.channel, " -from preset: ", presetIndex);
  //}
  for (int i = 0; i < rows.size(); i++) {
    Row row = rows.get(i);
    RowData rowData = presetData.rows.get(i);
    row.caption = rowData.caption;
    row.colour = rowData.colour;
    for (int j = 0; j < row.pots.size(); j++) {
      Pot pot = row.pots.get(j);
      PotData potData = rowData.pots.get(j);

      pot.potCaption = potData.potCaption;
      pot.sliderCaption = potData.sliderCaption;
      pot.potValue = potData.potValue;
      pot.sliderValue = potData.sliderValue;
      pot.cc_knob = potData.cc_knob;
      pot.cc_slider = potData.cc_slider;
      pot.potColour = potData.potColour;
      pot.sliderColour = potData.sliderColour;
      pot.updateUi();
    }
  }
}



//////////////////////////////////////////////////////////////////////
void saveData() {
  JSONObject json = new JSONObject();
  JSONArray jRows = new JSONArray();

  for (Row r : rows) {
    JSONObject jRow = new JSONObject();
    JSONArray jPots = new JSONArray();

    for (Pot p : r.pots) {
      JSONObject jPot = new JSONObject();
      jPot.setInt("value", p.potValue);
      jPot.setString("caption", p.potCaption);
      jPot.setInt("colour", p.potColour);
      jPots.append(jPot);
    }

    jRow.put("pots", jPots);
    jRows.append(jRow);
  }

  json.put("rows", jRows);
  saveJSONObject(json, "data/knobsData.json");
}

void loadData() {
  JSONObject json;

  json = loadJSONObject("data/knobsData.json");
  JSONArray jRows = json.getJSONArray("rows");

  for (int i = 0; i < jRows.size(); i++) {
    JSONObject jRow = jRows.getJSONObject(i);
    JSONArray jPots = jRow.getJSONArray("pots");

    Row currentRow = rows.get(i);

    for (int j = 0; j < jPots.size(); j++) {
      JSONObject jPot = jPots.getJSONObject(j);
      int value = jPot.getInt("value");
      String caption = jPot.getString("caption");
      int potColour = jPot.getInt("potColour");

      Pot currentPot = currentRow.pots.get(j);
      currentPot.potValue = value;
      currentPot.potCaption = caption;
      currentPot.potColour = potColour;
    }
  }
  //println("Total rows:", jRows.size());
  println("Total rows:", jRows);
  restoreValues(jRows);
}
void restoreValues(JSONArray jRows) {
  for (int i = 0; i < jRows.size(); i++) {
    JSONObject jRow = jRows.getJSONObject(i);
    JSONArray jPots = jRow.getJSONArray("pots");

    Row currentRow = rows.get(i);

    for (int j = 0; j < jPots.size(); j++) {
      Pot pot = currentRow.pots.get(j);

      //rows.get(i).pots.get(j).setValueOnly(pot.value);
      //rows.get(i).pots.get(j).caption=pot.caption;
      //rows.get(i).pots.get(j).setColour(pot.colour);


      rows.get(i).pots.get(j).restorePot(pot);
    }
  }
}
//////////////////////////////////////////////////////////
