class Row {
  int rowIndex;
  int x, y;

  int ancho, alto;
  boolean selected = false;
  int captionX, captionY, captionW, captionH;

  ArrayList<Pot> pots;

  String caption;
  boolean editingCaption = false;
  boolean _showingCursor=false;


  int selectedX, selectedY, selectedW, selectedH;

  ColorPicker colorPicker;
  int colour=-1;

  Row(int _index, int _x, int _y) {

    rowIndex = _index;
    x = _x;
    y = _y;

    caption = "Row_" + rowIndex;

    ancho = (g.potWidth + g.sep) * g.potsPerRow + g.sep2 - g.sep;
    alto = g.potHeight;

    captionX = x - g.potWidth- int(g.sep*0.5);
    captionY = y;
    captionW = g.potWidth  - g.sep*2;
    captionH = alto;

    selectedX=x - g.potWidth- g.sep*2;
    selectedY=y;
    selectedW=ancho+captionW+g.sep*2+1;
    selectedH=alto-1;

    pots = new ArrayList<Pot>();
    for (int i = 0; i < g.potsPerRow; i++) {
      int potIndex = i + rowIndex * g.potsPerRow;
      int potX = x + (g.potWidth + g.sep) * i;

      if (i >=  4) potX += g.sep2;

      int potY = y;
      Pot knob = new Pot(potIndex, potX, potY);
      pots.add(knob);
    }

    colorPicker = new ColorPicker(captionX, captionY + captionH, g.colores2);
  }

  void draw() {
    noStroke();
    fill(255);

    if (selected) {
      fill(getColorFromIndex(0, ACTIVE));
      int margen = 3;
      rect(selectedX, selectedY - margen, selectedW + margen * 2, selectedH + margen * 2, 5);
    }

    if (editingCaption) {
      fill(g.editingColor);
      rect( captionX, captionY, captionW, captionH, 5);
      fill(0);
      textAlign(LEFT, CENTER);
      text(caption+showingCursor(), captionX, captionY, captionW, captionH);
    } else {
      if (colour>-1) {
        fill(g.colores2[colour]);
      } else {
        fill(g.colores[1]);
      }

      rect( captionX, captionY, captionW, captionH, 5);
      fill(255);
      textAlign(CENTER, CENTER);
      text(caption, captionX, captionY, captionW, captionH);
    }

    for (int i = 0; i < pots.size(); i++) {
      pots.get(i).draw();
    }

    colorPicker.draw();
  }
  String showingCursor() {
    String cursor="_";
    if (frameCount%12==0) {
      _showingCursor=!_showingCursor;
    }
    if (!_showingCursor) {
      cursor="";
    }

    return cursor;
  }

  void mousePressed() {
    if (colorPicker.isShown()) {
      colorPicker.mousePressed();
      colour = colorPicker.getSelectedColor();
    } else if (clickedOnMe(captionX, captionY, captionW, captionH) && mouseButton == RIGHT) {
      colorPicker.show();
    } else {
      if (clickedOnMe(captionX, captionY, captionW, captionH)) {
        editingCaption=true;
      } else {
        if (clickedOnMe(x, y, ancho, alto)) {
          for (int i = 0; i < pots.size(); i++) {
            pots.get(i).mousePressed();
          }
        }
      }
    }
  }
  void keyPressed() {
    if (editingCaption) {
      if (key == ENTER || key == RETURN) {
        editingCaption = false;
      } else if (key == BACKSPACE) {
        if (caption.length() > 0) {
          caption = caption.substring(0, caption.length() - 1);
        }
      } else {
        if (Character.isLetterOrDigit(key) || key == ' ') {
          caption += key;
        }
      }
    }
  }
  void setMode(int _mode) {
    for (int i = 0; i < pots.size(); i++) {
      pots.get(i).setMode(_mode);
    }
  }
  void setSelected(boolean _selected) {
    selected=_selected;
    for (int i = 0; i < pots.size(); i++) {
      pots.get(i).setSelected(selected);
    }
  }
}
