/*

PRESET NO GUARDA EL CHANNELLLLLLLLLLLLLLLLLLLLLLLLL
 
 -botones izquierda: cambiar el toggle al de bien de la derecha
 -y el de bien de la izquierda usar para el toggle
  
 boton izquierdo: mostrar config celda!
 
 */

/// guardar json: si edito etiquetas o colores, grabar de una. Si cambio valores, no, ahi solo con "save"
/// cambiar el nombre de la clase pot a cell
/// poder tener diferentes paginas (presets? o archivos diferentes?)  presets > rows > pots (cells)
/// poder mover las filas arriba/abajo
/// que al presionar un boton tipo shift, que se seleccione la primer fila (master)

VariablesGlobales g;
import controlP5.*;
int KNOBS_GRID=0;
int SLIDERS_GRID=1;

ArrayList<Row> rows;
int selectedRow=0;

boolean firstBoot=true;

int footerY;

void setup() {

  size(768, 980);
  textAlign(CENTER, CENTER);
  g = new VariablesGlobales();
  cp5 = new ControlP5(this);

  rows= new ArrayList<Row>();
  int rowX=g.potWidth+g.sep*7;
  for (int i=0; i<g.totalRows; i++) {
    int rowY=165+(g.potHeight+g.sep)*i;
    Row row= new Row(i, rowX, rowY);
    rows.add(row);
  }
  footerY=rows.get(g.totalRows-1).pots.get(0).gridy+g.sep+int(g.potHeight*1.1);

  createMenuButtons();

  //addSliders();

  setupMidi();


  toggleMode();
  setRowSelected(0);

  setupPresets();
  restore(0);
  
  firstBoot=false;
}
void draw() {
  background(g.colores[0]);
  
  checkIfMidiIsPaused();

  drawMenuLabels();
  drawPresets();
  //drawFooter();
  for (int i=0; i<rows.size(); i++) {
    rows.get(i).draw();
  }
}

void mousePressed() {
  int rowEditingCaption=-1;
  for (int i=0; i<rows.size(); i++) {
    if (rows.get(i).editingCaption) {
      rowEditingCaption=i;
      break;
    }
  }
  for (int i=0; i<rows.size(); i++) {
    int potEditing=-1;

    for (int j=0; j<rows.get(i).pots.size(); j++) {
      if (rows.get(i).pots.get(j).isEditing) {
        potEditing=j;
        break;
      }
    }
    rows.get(i).mousePressed();
    if (potEditing>-1) {
      rows.get(i).pots.get(potEditing).setEditing(false);
      break;
    }
  }

  if (rowEditingCaption>-1) {
    rows.get(rowEditingCaption).editingCaption=false;
  }

  if (mouseY<presetsTop+g.potHeight) {
    for (int i=0; i<presets.size(); i++) {
      presets.get(i).mousePressed();
    }
  }

  /// SWAPPING ///////////////
  if (g.isSwappingIndex<0) {
    int potSwapping=-1;
    for (int i=0; i<rows.size(); i++) {
      for (int j=0; j<rows.get(i).pots.size(); j++) {
        if (rows.get(i).pots.get(j).isSwapping) {
          potSwapping=rows.get(i).pots.get(j).index;
          g.isSwappingIndex=potSwapping;
          break;
        }
        if (potSwapping>-1) {
          break;
        }
      }
    }

    if (potSwapping>-1) {
      for (int i=0; i<rows.size(); i++) {
        for (int j=0; j<rows.get(i).pots.size(); j++) {
          if (rows.get(i).pots.get(j).index==potSwapping) {
          } else {
            rows.get(i).pots.get(j).setSwappable(true);
          }
        }
      }
    }
  }
}

void setRowSelected(int index) {
  selectedRow=index;
  for (int i=0; i<rows.size(); i++) {
    rows.get(i).setSelected(false);
  }
  rows.get(selectedRow).setSelected(true);
}
void setPotValue(int _row, int _potIndex, int _value) {
  rows.get(_row).pots.get(_potIndex).setPotValue(_value);
}
void setSliderValue(int _row, int _potIndex, int _value) {
  //int cc=64+sliderIndex;
  //sendMidi(cc, value);
  rows.get(_row).pots.get(_potIndex).setSliderValue(_value);
}

void keyPressed() {

  int rowEditingCaption=-1;
  for (int i=0; i<rows.size(); i++) {
    if (rows.get(i).editingCaption) {
      rowEditingCaption=i;
      break;
    }
  }
  if (rowEditingCaption>-1) {
    rows.get(rowEditingCaption).keyPressed();
  } else {
    for (int i=0; i<rows.size(); i++) {
      for (int j=0; j<rows.get(i).pots.size(); j++) {
        if (rows.get(i).pots.get(j).isEditing) {
          rows.get(i).pots.get(j).keyPressed();
          break;
        }
      }
    }
  }
}
void mouseMoved() {
  if (mouseY<presetsTop+g.potHeight) {
    for (int i=0; i<presets.size(); i++) {
      presets.get(i).mouseMoved();
    }
  }
}
