ControlP5 cp5;
class Pot {
  int index;

  /// PROPERTIES:
  String potCaption;
  String sliderCaption;
  int potValue;
  int sliderValue;
  int cc_knob;
  int cc_slider;
  int potColour;
  int sliderColour;
  ///

  int x;
  int gridy;
  int ancho, alto;
  int anchoSlider, altoSlider;
  int altoCaption;

  Rectangle swapRect;

  Knob kn;
  Slider sl;

  int sliderIdBase = 500;

  int colorChangeTime = 0;
  int colorDuration = 300;
  boolean bgGridRestored = true;
  boolean bgFooterRestored = true;
  boolean firstBoot=true;
  boolean _showingCursor=false;
  boolean selected=false;
  int mode;
  boolean isEditing;
  boolean isSwapping;
  boolean swappable;

  Pot(int _index, int _x, int _y) {

    index = _index;
    cc_knob=_index;
    cc_slider=_index+64;
    x=_x;


    potValue = 64;
    potColour = 0;

    ancho = g.potWidth;
    alto = g.potHeight;
    anchoSlider=int(g.potWidth*0.3);
    altoSlider=int(g.potHeight*0.65);

    altoCaption = g.altoCaption;
    gridy=_y;
    //caption = "cc_" + index;
    potCaption="";
    sliderCaption="";

    //showingColors = false;

    crearKnob();
    crearSlider();
    setPotColour(0);
    setSliderColour(0);

    firstBoot=false;
    setEditing(false);


    int margen=6;
    swapRect=new Rectangle( x+margen, gridy+altoCaption*2+margen, ancho-margen*2, altoCaption);
  }
  void crearKnob() {
    String _nombre = "k_" + index;
    kn = cp5.addKnob(_nombre)
      .setId(index)
      .setBroadcast(false)
      .setRange(0, 127)
      //.setValue(value)
      .setPosition(x + ancho * 0.5-g.knobRadius, gridy + altoCaption)
      .setRadius(g.knobRadius)
      .setDecimalPrecision(0)
      .setNumberOfTickMarks(127)
      .setTickMarkLength(0)
      .snapToTickMarks(true)
      .hideTickMarks()
      .setDragDirection(Knob.VERTICAL)
      .setBroadcast(true)
      .setLabel("")
      ;

    setPotValue(potValue);
    kn.setValueLabel("");
  }
  void crearSlider() {
    String _nombre = "s_" + index;
    sl=cp5.addSlider(_nombre)
      .setId(index+64)
      .setRange(0, 127)
      .setPosition(x + ancho * 0.5-anchoSlider*0.5, gridy + altoCaption)
      .setDecimalPrecision(0)
      .setLabel("")
      .setSize(anchoSlider, altoSlider)
      ;
    sl.getValueLabel().align(ControlP5.RIGHT, ControlP5.TOP).setPaddingX(int(anchoSlider*1.30));
    sl.setValueLabel("");
  }


  void draw() {
    int y=gridy;

    /// BACKGROUND
    noStroke();
    fill(g.colores[1]);
    rect(x, y, ancho, alto, 5);
    if (!bgGridRestored) {
      //fill(getColorFromIndex(0, ACTIVE), 70);
      //rect(x, y, ancho, alto, 5);
    }
    if (swappable) {
      fill(getColorFromIndex(2, ACTIVE), 70);
      rect(x, y, ancho, alto, 5);
    }

    /// footer
    if (selected) {
      fill(g.colores[1]);
      rect(x, footerY, g.potWidth, g.potHeight, 5);
      if (!bgFooterRestored) {
        //fill(getColorFromIndex(0, ACTIVE), 70);
        //rect(x, footerY, g.potWidth, g.potHeight, 5);
      }
    }

    /// CAPTION
    String caption="";
    String captionFooter="";
    if (mode==KNOBS_GRID) {
      caption=potCaption;
      captionFooter=sliderCaption;
    } else if (mode==SLIDERS_GRID) {
      caption=sliderCaption;
      captionFooter=potCaption;
    }
    fill(255);
    textAlign(CENTER, CENTER);
    text(captionFooter, x, footerY-3, ancho, altoCaption-3);
    if (isEditing) {
      fill(g.editingColor);
      rect(x, y, ancho, altoCaption, 5, 5, 0, 0);
      fill(0);
      textAlign(LEFT, CENTER);
      text(caption+showingCursor(), x, y-3, ancho, altoCaption-3);
    } else {
      text(caption, x, y-3, ancho, altoCaption-3);
    }

    /// GRID COLOR
    if (isEditing) {
      int totalColores = g.colores2.length;
      int totalAncho=int(totalColores/2);
      float w = ancho / totalAncho;
      noStroke();
      for (int i = 0; i < totalAncho; i++) {
        fill(g.colores2[i]);
        rect(x +1+ w * i, y+altoCaption+2, w, altoCaption*0.5);
      }
      for (int i = totalAncho; i < totalColores; i++) {
        fill(g.colores2[i]);
        rect(x +1+ w * (i-totalAncho), y+altoCaption+altoCaption*0.45+2, w, altoCaption*0.5);
      }
    }

    if (!bgGridRestored || !bgFooterRestored) {
      if (millis() - colorChangeTime > colorDuration) {
        kn.setColorForeground(getColorFromIndex(potColour, FOREGROUND));
        sl.setColorForeground(getColorFromIndex(sliderColour, FOREGROUND));
        bgGridRestored = true;
        bgFooterRestored = true;

        sl.setValueLabel("");
        kn.setValueLabel("");
      }
    }

    /// show index CC
    String labelCCFooter="", labelCCGrid="";
    if (mode==SLIDERS_GRID) {
      labelCCFooter=str(index);
      labelCCGrid=str(index+64);
    } else if (mode==KNOBS_GRID) {
      labelCCFooter=str(index+64);
      labelCCGrid=str(index);
    }
    textAlign(RIGHT, BOTTOM);
    fill(150);
    if (!bgGridRestored) fill(255);
    text(labelCCGrid, x+ancho-5, y+alto-3);
    if (selected) {
      fill(150);
      if (!bgFooterRestored) fill(255);
      text(labelCCFooter, x+ancho-5, footerY+alto-3);
    }

    /// SWAP
    if (isEditing) {
      //int margen=6;
      fill(getColorFromIndex(0, FOREGROUND));
      //rect( x+margen, y+altoCaption*2+margen, ancho-margen*2, altoCaption, 5);
      swapRect.draw();
      fill(255);
      textAlign(CENTER, CENTER);

      //text("SWAP", x+margen, y+altoCaption*2+margen*0.5, ancho-margen*2, altoCaption);
      text("SWAP", swapRect.x, swapRect.y-3, swapRect.w, swapRect.h);
    }
  }
  //////////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////
  String showingCursor() {
    String cursor="_";
    if (frameCount%12==0) {
      _showingCursor=!_showingCursor;
    }
    if (!_showingCursor) {
      cursor="";
    }

    return cursor;
  }
  //void showColors(boolean _show) {
  //  showingColors = _show;
  //}
  void setPotColour(int _index) {
    potColour = _index;
    kn.setColorActive(getColorFromIndex(potColour, ACTIVE))
      .setColorForeground(getColorFromIndex(potColour, FOREGROUND))
      .setColorBackground(getColorFromIndex(potColour, BACKGROUND ));
  }
  void setSliderColour(int _index) {
    sliderColour = _index;
    sl.setColorActive(getColorFromIndex(sliderColour, ACTIVE))
      .setColorForeground(getColorFromIndex(sliderColour, FOREGROUND))
      .setColorBackground(getColorFromIndex(sliderColour, BACKGROUND ));
  }

  void restorePot(Pot pot) {
    setPotValueOnly(pot.potValue);
    potCaption=pot.potCaption;
    setPotColour(pot.potColour);
  }
  void setPotValue(int _val) {
    int difference = abs(potValue - _val);
    if (difference <= 3) {
      potValue=_val;
      kn.setValue(_val);

      if (!firstBoot) {
        kn.setColorForeground(getColorFromIndex(potColour, ACTIVE));
      }
    }
  }
  void setPotValueOnly(int _val) {
    potValue=_val;
    kn.setValue(_val);
  }
  void setSliderValue(int _val) {
    int difference = abs(sliderValue - _val);
    if (difference <= 5) {
      sliderValue=_val;
      sl.setValue(_val);
      if (!firstBoot) {
        sl.setColorForeground(getColorFromIndex(sliderColour, ACTIVE));
      }
    }
  }
  void setSliderValueOnly(int _val) {
    sliderValue=_val;
    sl.setValue(_val);
  }
  void setPickedColor() {
    if (mouseY>gridy+altoCaption && mouseY<gridy+altoCaption*2) {
      int newIndex = int(map(mouseX, x, x + ancho, 0, g.colores2.length/2));
      if (mouseY>=gridy+altoCaption*0.5+altoCaption+3) {
        newIndex+=int(g.colores2.length*0.5);
      }
      if (mode==SLIDERS_GRID) {
        setSliderColour(newIndex);
      } else if (mode==KNOBS_GRID) {
        setPotColour(newIndex);
      }
    }
  }
  void setEditing(boolean _editing) {
    isEditing=_editing;
    if (isEditing) {
      if (mode==SLIDERS_GRID) {
        sl.hide();
      }
      if (mode==KNOBS_GRID) {
        kn.hide();
      }
    } else {
      if (mode==SLIDERS_GRID) {
        sl.show();
      }
      if (mode==KNOBS_GRID) {
        kn.show();
      }
    }
  }
  void updateUi() {
    setPotValueOnly(potValue);
    setPotColour(potColour);
    //setSliderValueOnly(sliderValue);
    setSliderColour(sliderColour);

    colorChangeTime=millis();
    bgFooterRestored=false;
    bgGridRestored=false;
  }
  ///////////////////////////////////////////////////////////////////////
  void mousePressed() {
    if (clickedOnMe(x, gridy, ancho, alto)) {
      if (mouseButton == RIGHT) {
        if (!swappable)
          setEditing(true);
      } else if (mouseButton == LEFT) {
        if (swappable==true) {
          //println(index, swappable);
          swapPots(index, g.isSwappingIndex);
        } else {
          if (isEditing) {
            setPickedColor();
            //setEditing(false);
            if (swapRect.contains(mouseX, mouseY)) {
              //println(index);
              isSwapping=true;
            }
          }
        }

        setEditing(false);
      }
    }
  }
  void keyPressed() {
    if (isEditing) {
      String caption="";
      if (mode==KNOBS_GRID) {
        caption=potCaption;
      } else if (mode==SLIDERS_GRID) {
        caption=sliderCaption;
      }

      if (key == ENTER || key == RETURN) {
        setEditing(false);
        return;
      } else if (key == BACKSPACE) {
        if (caption.length() > 0) {
          caption = caption.substring(0, caption.length() - 1);
        }
      } else {
        if (Character.isLetterOrDigit(key) || key == ' ') {
          caption += key;
        }
      }
      if (mode==KNOBS_GRID) {
        potCaption=caption;
      } else if (mode==SLIDERS_GRID) {
        sliderCaption=caption;
      }
    }
  }
  void setMode(int _mode) {
    mode=_mode;
    if (_mode==SLIDERS_GRID) {
      sl.show();
      sl.setPosition(x + ancho * 0.5-anchoSlider*0.5, gridy + altoCaption);
      if (!selected) {
        kn.hide();
      }
    }
    if (_mode==KNOBS_GRID) {
      kn.show();
      kn.setPosition(x + ancho * 0.5-g.knobRadius, gridy + altoCaption);
      if (!selected) {
        sl.hide();
      }
    }
  }
  void setSelected(boolean _selected) {
    selected=_selected;
    if (mode==SLIDERS_GRID) {
      if (selected) {
        kn.show();
        kn.setPosition(x + ancho * 0.5-g.knobRadius, footerY+gridy*0.0 + altoCaption);
      } else {
        kn.hide();
      }
    }
    if (mode==KNOBS_GRID) {
      if (selected) {
        sl.show();
        sl.setPosition(x + ancho * 0.5-anchoSlider*0.5, footerY+gridy*0.0 + altoCaption);
      } else {
        sl.hide();
      }
    }
  }
  void setSwappable(boolean _value) {
    swappable=_value;
  }
}


/////////////////////////
void controlEvent(ControlEvent theEvent) {
  if (!firstBoot) {

    if (theEvent.isController()) {
      int id = theEvent.getId();
      String nombre = theEvent.getController().getName();
      int value = int(theEvent.getController().getValue());
      //println(id, nombre, value);

      if (id>-1 && id<64) {
        /// KNOBS

        //println(id % g.potsPerRow);
        int potIndex=id % g.potsPerRow;
        int rowIndex=id/g.potsPerRow;
        Pot pot=rows.get(rowIndex).pots.get(potIndex);
        if (!(pot.isSwapping || pot.swappable)) {
          pot.colorChangeTime=millis();
          if (pot.mode==SLIDERS_GRID) {
            pot.bgFooterRestored=false;
          }
          if (pot.mode==KNOBS_GRID) {
            pot.bgGridRestored=false;
          }
          rows.get(rowIndex).pots.get(potIndex).potValue=value;

          sendMidi(pot.cc_knob, value);
        }
      } else if (id>=64) {
        /// sLIDERS

        int potIndex=id % g.potsPerRow;
        int rowIndex=id/g.potsPerRow-8;
        Pot pot=rows.get(rowIndex).pots.get(potIndex);
        if (!(pot.isSwapping || pot.swappable)) {
          pot.colorChangeTime=millis();
          if (pot.mode==SLIDERS_GRID) {
            pot.bgGridRestored=false;
          }
          if (pot.mode==KNOBS_GRID) {
            pot.bgFooterRestored=false;
          }
          rows.get(rowIndex).pots.get(potIndex).sliderValue=value;

          sendMidi(pot.cc_slider, value);
        }
      }
    }
  }
}
//////////////
void swapPots(int _index1, int _index2) {
  println("Swapping " + _index1 + " with " + _index2);
  int potIndex1=_index1 % g.potsPerRow;
  int rowIndex1=_index1/g.potsPerRow;
  Pot pot1 = rows.get(rowIndex1).pots.get(potIndex1);
  int potIndex2=_index2 % g.potsPerRow;
  int rowIndex2=_index2/g.potsPerRow;

  Pot pot2 = rows.get(rowIndex2).pots.get(potIndex2);

  // Guardar los valores del pot1 en variables temporales
  String tempPotCaption = pot1.potCaption;
  String tempSliderCaption = pot1.sliderCaption;
  int tempPotValue = pot1.potValue;
  int tempSliderValue = pot1.sliderValue;
  int tempCc_knob = pot1.cc_knob;
  int tempCc_slider = pot1.cc_slider;
  int tempPotColour = pot1.potColour;
  int tempSliderColour = pot1.sliderColour;

  if (g.mode==KNOBS_GRID) {
    pot1.potCaption = pot2.potCaption;
    pot2.potCaption = tempPotCaption;
    pot1.potValue = pot2.potValue;
    pot2.potValue = tempPotValue;
    pot1.potColour = pot2.potColour;
    pot2.potColour = tempPotColour;
    pot1.cc_knob = pot2.cc_knob;
    pot2.cc_knob = tempCc_knob;

    //pot1.setPotValueOnly(pot1.potValue);
    //pot2.setPotValueOnly(pot2.potValue);
    //pot1.setPotColour(pot1.potColour);
    //pot2.setPotColour(pot2.potColour);
    pot1.updateUi();
    pot2.updateUi();
  } else if (g.mode==SLIDERS_GRID) {
    pot1.sliderCaption = pot2.sliderCaption;
    pot2.sliderCaption = tempSliderCaption;
    pot1.sliderValue = pot2.sliderValue;
    pot2.sliderValue = tempSliderValue;
    pot1.cc_slider = pot2.cc_slider;
    pot2.cc_slider = tempCc_slider;
    pot1.sliderColour = pot2.sliderColour;
    pot2.sliderColour = tempSliderColour;

    //pot1.setSliderValueOnly(pot1.sliderValue);
    //pot2.setSliderValueOnly(pot2.sliderValue);
    //pot1.setSliderColour(pot1.sliderColour);
    //pot2.setSliderColour(pot2.sliderColour);
    pot1.updateUi();
    pot2.updateUi();
  }

  setPotsUnswappables();
}
void setPotsUnswappables() {
  g.isSwappingIndex=-1;
  for (int i=0; i<rows.size(); i++) {
    for (int j=0; j<rows.get(i).pots.size(); j++) {
      rows.get(i).pots.get(j).isSwapping=false;
      rows.get(i).pots.get(j).setSwappable(false);
    }
  }
}
